def calculate_rectangle_area(length, width):
    return length * width

def main():
    length = 5  # Length of the rectangle
    width = 3   # Width of the rectangle
    
    area = calculate_rectangle_area(length, width)
    
    print("The area of the rectangle is:", area)

if __name__ == "__main__":
    main()
