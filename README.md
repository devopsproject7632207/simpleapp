# SimpleApp



## setup a gitlab repository

Four files are in the gitlab repository I created: 1. program1.py (a Python application for basic math operations) 2. The gitlab ci/cd pipeline code is contained in the.gitlab-cli.yml file. 3. Jenkinsfile (which includes pipeline code for Jenkins) 4. The step-by-step documentation file, README.md
The "SimpleApp" repository was started using a straightforward README page.


## Simple Python Application

A simple python application was created which performs basic calculation of reactange of area.
the file is named as "program1.py". 

## jenkins project setup

The jenkins project was setup to integrate with your GitLab repository.

## Creation of Jenkinsfile

According to the given format of the Jenkinsfile was created. The pipeline has four stages : 1. Checkout 2. Build 3. Test 4. Deploy.
As the python project doesnt have the build stage therefore no commands are written under the build stage only simple echo message is printed.

## Creation of .gitlab.cli.yml file

This configuration will trigger the Jenkins pipeline on each commit to the main branch of your GitLab repository, sending a POST request to the Jenkins job URL with the appropriate token.
As well as the gitlab integration set up is all done to trigger the jenkins pipeline on each commit to the repository.

## documentation

Proper documentation is done of the steps done to complete the assignment.

